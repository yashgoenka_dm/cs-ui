/**
*  csApp user side application
*
*/
angular.module('csApp.user', [

	'csApp.user.portfolio',
	'ui.router'

])


.config(function($stateProvider, $urlRouterProvider) {
	
})