/**
*  portfolio dashboard
*
*/
angular.module('csApp.user.portfolio', [
	'csApp.user.portfolio.controllers',
	'csApp.user.portfolio.services',
	'csApp.user.portfolio.directives'
])