'use strict';
// Source: components/user/app.user.js
/**
*  csApp user side application
*
*/
angular.module('csApp.user', [

	'csApp.user.portfolio',
	'ui.router'

])


.config(function($stateProvider, $urlRouterProvider) {
	
})
// Source: components/user/components/portfolio/app.user.portfolio.js
/**
*  portfolio dashboard
*
*/
angular.module('csApp.user.portfolio', [
	'csApp.user.portfolio.controllers',
	'csApp.user.portfolio.services',
	'csApp.user.portfolio.directives'
])
// Source: components/user/components/portfolio/controllers/portfolioController.js
/**
* ` portfolio controller 
*/
angular.module('csApp.user.portfolio.controllers', [])


.controller('portfolioController', ['$rootScope', function($rootScope){
	
	var pc = this;

	


	
	return pc;

}])
// Source: components/user/components/portfolio/directives/portfolioDirectives.js
/*
*
* portfolio directives
*/
angular.module('csApp.user.portfolio.directives', [])

// Source: components/user/components/portfolio/services/portfolioServices.js
/*
*
* portfolio service
*/
angular.module('csApp.user.portfolio.services', [])

.service('portfolioService', ['$http', function($http){
	

	var service = {}


	// service.methodName = function(){


	// }


	return service;


}])